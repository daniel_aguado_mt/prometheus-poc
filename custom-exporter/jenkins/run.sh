#!/bin/bash
set -e

PARAMS=""

if [ -z "$JENKINS_SERVER" ]; then
  PARAMS="--jenkins ${JENKINS_SERVER} "
fi

if [ -z "$JENKINS_USER" ]; then
  PARAMS="--user ${JENKINS_USER} "
fi

if [ -z "$JENKINS_PASSWORD" ]; then
  PARAMS="--password ${JENKINS_PASSWORD} "
fi

if [ -z "$EXPORTER_PORT" ]; then
  PARAMS="--port ${EXPORTER_PORT:-9118} "
fi

python -u jenkins_exporter.py ${PARAMS}
